# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

import mysql.connector

class TgddPipeline:

    def __init__(self):
        self.create_connection()
        self.create_table()

    def create_connection(self):
        self.conn = mysql.connector.connect(
            host= 'localhost',
            user= 'root',
            database='tgdd'
        )
        self.curr = self.conn.cursor()

    def create_table(self):
        self.curr.execute("""drop table if exists tgdd""")
        self.curr.execute("""create table tgdd(
                            product_name varchar(100),
                            product_price varchar(20),
                            product_image varchar(200),
                            product_details varchar(254))""")

    def process_item(self, item, spider):
        self.store_db(item)
        return item

    def store_db(self, item):
        self.curr.execute("""insert into tgdd values(%s,%s,%s,%s)""",(
            item['product_name'][0],
            item['product_price'][0],
            item['product_image'][0],
            item['product_details'][0]
        ))
        self.conn.commit()
