# -*- coding: utf-8 -*-
import scrapy
from ..items import TgddItem

class DidongSpider(scrapy.Spider):
    name = 'didong'
    allowed_domains = ['https://www.thegioididong.com/']
    start_urls = []
    list_brand =['apple-iphone', 'samsung', 'oppo', 'xiaomi', 'vivo', 'realme', 'vsmart']

    for i in range(len(list_brand)):
        start_urls.append('https://www.thegioididong.com/'+'dtdd'+'-'+list_brand[i])

    def parse(self, response):
        items = TgddItem()
        all_item = response.css(".item")
        for item in all_item:
            product_name = item.css("h3::text").extract()
            product_price = item.css(".price strong::text").extract()
            product_image = item.css(".item img::attr(src)").extract()
            product_details = item.css(".bginfo span::text").extract()

            items['product_name'] = product_name
            items['product_price'] = product_price
            items['product_image'] = product_image
            items['product_details'] = product_details

            yield items


