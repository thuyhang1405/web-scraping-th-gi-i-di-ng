-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th4 28, 2020 lúc 11:06 AM
-- Phiên bản máy phục vụ: 10.4.6-MariaDB
-- Phiên bản PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `tgdd`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tgdd`
--

CREATE TABLE `tgdd` (
  `product_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_price` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_image` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_details` varchar(254) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tgdd`
--

INSERT INTO `tgdd` (`product_name`, `product_price`, `product_image`, `product_details`) VALUES
('iPhone 8 Plus 64GB', '14.490.000₫', 'https://cdn.tgdd.vn/Products/Images/42/114110/iphone-8-plus-hh-600x600-600x600.jpg', 'Màn hình: 5.5\", Retina HD'),
('iPhone 11 Pro Max 512GB', '40.990.000₫', 'https://cdn.tgdd.vn/Products/Images/42/210654/iphone-11-pro-max-512gb-gold-600x600.jpg', 'Màn hình: 6.5\", Super Retina XDR'),
('OPPO A9 (2020)', '5.990.000₫', 'https://cdn.tgdd.vn/Products/Images/42/202028/oppo-a9-600x600-trang-600x600.jpg', 'Màn hình: 6.5\", HD+'),
('OPPO Find X2', '23.990.000₫', 'https://cdn.tgdd.vn/Products/Images/42/198150/oppo-find-x2-blue-600x600-600x600.jpg', 'Màn hình: 6.78\", Quad HD+ (2K+)'),
('iPhone 11 Pro Max 256GB', '37.990.000₫', 'https://cdn.tgdd.vn/Products/Images/42/210653/iphone-11-pro-max-256gb-black-600x600.jpg', 'Màn hình: 6.5\", Super Retina XDR'),
('iPhone 11 Pro 256GB', '34.990.000₫', 'https://cdn.tgdd.vn/Products/Images/42/210655/iphone-11-pro-256gb-black-600x600.jpg', 'Màn hình: 5.8\", Super Retina XDR'),
('iPhone 11 Pro Max 64GB', '33.990.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.5\", Super Retina XDR'),
('iPhone Xs Max 256GB', '33.990.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.5\", Super Retina'),
('iPhone 11 Pro 64GB', '30.990.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 5.8\", Super Retina XDR'),
('OPPO Reno2 F', '8.990.000₫', 'https://cdn.tgdd.vn/Products/Images/42/209800/oppo-reno2-f-600x600-600x600.jpg', 'Màn hình: 6.5\", Full HD+'),
('OPPO A91', '6.490.000₫', 'https://cdn.tgdd.vn/Products/Images/42/217287/oppo-a91-trang-600x600-600x600.jpg', 'Màn hình: 6.4\", Full HD+'),
('OPPO A5 (2020) 128GB', '4.790.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.5\", HD+'),
('OPPO A31 (6GB/128GB)', '5.290.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.5\", HD+'),
('iPhone Xs Max 64GB', '27.990.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.5\", Super Retina'),
('iPhone 11 256GB', '24.990.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.1\", Liquid Retina'),
('iPhone Xs 256GB', '24.990.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 5.8\", Super Retina'),
('iPhone 11 128GB', '23.990.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.1\", Liquid Retina'),
('iPhone 11 64GB', '21.990.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.1\", Liquid Retina'),
('OPPO A31 (4GB/128GB)', '4.490.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.5\", HD+'),
('OPPO A12 (4GB/64GB)', '3.990.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.22\", HD+'),
('OPPO A12 (3GB/32GB)', '3.490.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.22\", HD+'),
('OPPO A5s', '3.290.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.2\", HD+'),
('OPPO A1K', '2.690.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.1\", HD+'),
('iPhone Xs 64GB', '20.990.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 5.8\", Super Retina'),
('iPhone 7 Plus 32GB', '10.990.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 5.5\", Retina HD'),
('iPhone 7 32GB', '9.990.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 4.7\", Retina HD'),
('Xiaomi Redmi Note 9S', '5.990.000₫', 'https://cdn.tgdd.vn/Products/Images/42/214924/xiaomi-redmi-note-9s-4gb-green-400x460-600x600.jpg', 'Màn hình: 6.67\", Full HD+'),
('Vivo V17 Pro', '8.990.000₫', 'https://cdn.tgdd.vn/Products/Images/42/210243/vivo-v17-pro-blue-1-600x600.jpg', 'Màn hình: 6.44\", Full HD+'),
('Realme C3', '2.990.000₫', 'https://cdn.tgdd.vn/Products/Images/42/218361/realme-c3-do-600x600-600x600.jpg', 'Màn hình: 6.5\", HD+'),
('Samsung Galaxy A71', '10.090.000₫', 'https://cdn.tgdd.vn/Products/Images/42/210246/samsung-galaxy-a71-blue-600x600-1-600x600.jpg', 'Màn hình: 6.7\", Full HD+'),
('Samsung Galaxy S10 Lite', '13.990.000₫', 'https://cdn.tgdd.vn/Products/Images/42/194251/samsung-galaxy-s10-lite-blue-thumb-600x600.jpg', 'Màn hình: 6.7\", Full HD+'),
('Samsung Galaxy Fold', '50.000.000₫', 'https://cdn.tgdd.vn/Products/Images/42/198158/samsung-galaxy-fold-black-600x600.jpg', 'Màn hình: Chính 7.3\" & Phụ 4.6\", Quad HD (2K)'),
('Vsmart Joy 3 (3GB/32GB)', '2.690.000₫', 'https://cdn.tgdd.vn/Products/Images/42/217920/vsmart-joy-3-tim-600x600-600x600.jpg', 'Màn hình: 6.5\", HD+'),
('Vsmart Active 3 (6GB/64GB)', '3.990.000₫', 'https://cdn.tgdd.vn/Products/Images/42/217438/vsmart-active-3-6gb-emerald-green-600x600-600x600.jpg', 'Màn hình: 6.39\", Full HD+'),
('Vsmart Joy 3 (4GB/64GB)', '3.290.000₫', 'https://cdn.tgdd.vn/Products/Images/42/219208/vsmart-joy-3-4gb-den-600x600-600x600.jpg', 'Màn hình: 6.5\", HD+'),
('Vsmart Joy 3 (2GB/32GB)', '2.290.000₫', 'https://cdn.tgdd.vn/Products/Images/42/217921/vsmart-joy-3-2gb-tim-600x600-600x600.jpg', 'Màn hình: 6.5\", HD+'),
('Vsmart Star 3', '1.690.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.09\", HD+'),
('Xiaomi Mi Note 10 Pro', '13.490.000₫', 'https://cdn.tgdd.vn/Products/Images/42/213590/xiaomi-mi-note-10-pro-black-600x600.jpg', 'Màn hình: 6.47\", Full HD+'),
('Xiaomi Mi 9 SE', '7.490.000₫', 'https://cdn.tgdd.vn/Products/Images/42/198394/xiaomi-mi-9-se-1-600x600.jpg', 'Màn hình: 5.97\", Full HD+'),
('Vivo V19', '8.990.000₫', 'https://cdn.tgdd.vn/Products/Images/42/217859/vivo-v19-xanh-600x600-600x600.jpg', 'Màn hình: 6.44\", Full HD+'),
('Realme 6 Pro', '7.990.000₫', 'https://cdn.tgdd.vn/Products/Images/42/214645/realme-6-pro-600x600-2-600x600.jpg', 'Màn hình: 6.6\", Full HD+'),
('Samsung Galaxy Z Flip', '36.000.000₫', 'https://cdn.tgdd.vn/Products/Images/42/213022/samsung-galaxy-z-flip-den-600x600-600x600.jpg', 'Màn hình: 6.7\", Quad HD (2K)'),
('Vsmart Star', '1.290.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 5.7\", HD+'),
('Vsmart Bee 3', '1.390.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.0\", HD+'),
('Vsmart Bee', '990.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 5.45\", HD+'),
('Xiaomi Redmi Note 8 Pro (6GB/128GB)', '5.990.000₫', 'https://cdn.tgdd.vn/Products/Images/42/214418/xiaomi-redmi-note-8-pro-6gb-128gb-white-600x600.jpg', 'Màn hình: 6.53\", Full HD+'),
('Vivo S1 Pro', '6.490.000₫', 'https://cdn.tgdd.vn/Products/Images/42/202862/vivo-s1-pro-white-600x600.jpg', 'Màn hình: 6.38\", Full HD+'),
('Vivo V15 128GB', '5.490.000₫', 'https://cdn.tgdd.vn/Products/Images/42/199041/vivo-v15-quanghai-600x600.jpg', 'Màn hình: 6.53\", Full HD+'),
('Realme 5 Pro (8GB/128GB)', '5.990.000₫', 'https://cdn.tgdd.vn/Products/Images/42/211163/realme-5-pro-8gb-purple-600x600.jpg', 'Màn hình: 6.3\", Full HD+'),
('Realme 6 (8GB/128GB)', '6.990.000₫', 'https://cdn.tgdd.vn/Products/Images/42/219895/realme-6-8gb-600x600-600x600.jpg', 'Màn hình: 6.5\", Full HD+'),
('Realme 6 (4GB/128GB)', '5.990.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.5\", Full HD+'),
('Realme 5 Pro (4GB/128GB)', '4.790.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.3\", Full HD+'),
('Realme 5 (4GB/128GB)', '3.990.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.5\", HD+'),
('Realme 5s', '4.190.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.5\", HD+'),
('Samsung Galaxy S20 Ultra', '27.490.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.9\", Quad HD+ (2K+)'),
('Samsung Galaxy S20+', '23.990.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.7\", Quad HD+ (2K+)'),
('Samsung Galaxy Note 10', '22.990.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.3\", Full HD+'),
('Samsung Galaxy S20', '21.490.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.2\", Quad HD+ (2K+)'),
('Samsung Galaxy S10+', '19.990.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.4\", Quad HD+ (2K+)'),
('Samsung Galaxy A80', '14.990.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.7\", Full HD+'),
('Xiaomi Redmi Note 8 Pro (6GB/64GB)', '5.990.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.53\", Full HD+'),
('Xiaomi Redmi Note 8 (4GB/128GB)', '5.490.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.3\", Full HD+'),
('Xiaomi Redmi Note 8 (4GB/64GB)', '4.390.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.3\", Full HD+'),
('Xiaomi Redmi Note 7 (4GB/64GB)', '3.990.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.3\", Full HD+'),
('Xiaomi Mi A3', '3.790.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.01\", HD+'),
('Xiaomi Redmi Note 8 (3GB/32GB)', '3.790.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.3\", Full HD+'),
('Vivo Y50', '6.290.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.53\", Full HD+'),
('Vivo S1', '5.490.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.38\", Full HD+'),
('Realme 5i (4GB/64GB)', '3.690.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.52\", HD+'),
('Realme 5 (3GB/64GB)', '3.590.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.5\", HD+'),
('Realme 5i (3GB/32GB)', '3.290.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.52\", HD+'),
('Realme C3i', '2.590.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.5\", HD+'),
('Realme C2 (2GB/32GB)', '2.090.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.1\", HD+'),
('Samsung Galaxy Note 10 Lite', '13.990.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.7\", Full HD+'),
('Xiaomi Redmi 7 (3GB/32GB)', '2.990.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.26\", HD+'),
('Vivo Y19', '4.690.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.53\", Full HD+'),
('Samsung Galaxy A70', '9.290.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.7\", Full HD+'),
('Samsung Galaxy A51 (8GB/128GB)', '8.390.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.5\", Full HD+'),
('Xiaomi Redmi 8 (4GB/64GB)', '2.990.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.22\", HD+'),
('Xiaomi Redmi 7 (2GB/16GB)', '2.590.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.26\", HD+'),
('Xiaomi Redmi 8 (3GB/32GB)', '2.690.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.22\", HD+'),
('Xiaomi Redmi 8A', '2.590.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.22\", HD+'),
('Vivo Y17', '4.090.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.35\", HD+'),
('Vivo U10', '3.790.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.35\", HD+'),
('Samsung Galaxy A51 (6GB/128GB)', '7.590.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.5\", Full HD+'),
('Xiaomi Redmi Go 16GB', '1.790.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 5\", HD'),
('Vivo Y15', '3.790.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.35\", HD+'),
('Vivo Y12', '3.590.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.35\", HD+'),
('Vivo Y11', '2.990.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.35\", HD+'),
('Vivo Y93', '2.690.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.2\", HD+'),
('Samsung Galaxy A50 128GB', '7.290.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.4\", Full HD+'),
('Samsung Galaxy A50s', '6.490.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.4\", Full HD+'),
('Vivo Y91C', '2.390.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.22\", HD+'),
('Samsung Galaxy A31', '6.490.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.4\", Full HD+'),
('Samsung Galaxy A50 64GB', '6.490.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.4\", Full HD+'),
('Samsung Galaxy A30s', '5.590.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.4\", HD+'),
('Samsung Galaxy A20s 64GB', '5.390.000₫', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX6+vqsEtnpAAAACklEQVQI12NgAAAAAgAB4iG8MwAAAABJRU5ErkJggg==', 'Màn hình: 6.5\", HD+');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
